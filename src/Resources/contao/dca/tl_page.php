<?php

  $GLOBALS['TL_DCA']['tl_page']['palettes']['__selector__'][] = 'useManifest';
  $GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback'] = str_replace(
    'robotsTxt;',
    'robotsTxt,useManifest;',
    $GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback']
  );

  $GLOBALS['TL_DCA']['tl_page']['subpalettes']['useManifest'] = 'manifest_name,manifest_shortname,manifest_description,manifest_background_color,manifest_theme_color,manifest_display,manifest_orientation,manifest_icons';

  $GLOBALS['TL_DCA']['tl_page']['fields']['useManifest'] = array(
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true,'tl_class'=>'m12'),
    'sql'                     => "char(1) NOT NULL default ''"
  );
  $GLOBALS['TL_DCA']['tl_page']['fields']['manifest_name'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );
  $GLOBALS['TL_DCA']['tl_page']['fields']['manifest_description'] = array(
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('tl_class'=>'long clr'),
    'sql'                     => "text NULL"
  );
  $GLOBALS['TL_DCA']['tl_page']['fields']['manifest_shortname'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );
  $GLOBALS['TL_DCA']['tl_page']['fields']['manifest_background_color'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>6, 'colorpicker'=>true, 'isHexColor'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
    'sql'                     => "varchar(64) NOT NULL default ''"
  );
  $GLOBALS['TL_DCA']['tl_page']['fields']['manifest_theme_color'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>6, 'colorpicker'=>true, 'isHexColor'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
    'sql'                     => "varchar(64) NOT NULL default ''"
  );
  $GLOBALS['TL_DCA']['tl_page']['fields']['manifest_display'] = array(
    'exclude'                 => true,
    'inputType'               => 'select',
    'eval'                    => array('tl_class'=>'w50','mandatory'=>false,'includeBlankOption'=>true),
    'options'                 => array('fullscreen','standalone','minimal-ui','browser'),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );
  $GLOBALS['TL_DCA']['tl_page']['fields']['manifest_orientation'] = array(
    'exclude'                 => true,
    'inputType'               => 'select',
    'eval'                    => array('tl_class'=>'w50','mandatory'=>false,'includeBlankOption'=>true),
    'options'                 => array('any','natural','landscape','landscape-primary','landscape-secondary','portrait','portrait-primary','portrait-secondary'),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );
  $GLOBALS['TL_DCA']['tl_page']['fields']['manifest_icons'] = array(
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('tl_class'=>'clr','multiple'=>true, 'fieldType'=>'checkbox', 'filesOnly'=>true,'extensions'=>Config::get('validImageTypes')),
    'sql'                     => "blob NULL"
  );
