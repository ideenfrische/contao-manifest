<?php
namespace ideenfrische\ContaoManifestBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\PageRegular;
use Contao\LayoutModel;
use Contao\PageModel;

/**
 * @Hook("generatePage")
 */
class GeneratePageListener
{
    public function __invoke(PageModel $pageModel, LayoutModel $layout, PageRegular $pageRegular): void
    {
      $rootPage = \Contao\PageModel::findPublishedFallbackByHostname(
          \Contao\Environment::get('host'),
          ['fallbackToEmpty' => true]
      );
      if($rootPage->useManifest){
        $layout->head .= '<link rel="manifest" href="site.webmanifest">';
      }
    }
}
