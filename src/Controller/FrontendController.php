<?php
namespace ideenfrische\ContaoManifestBundle\Controller;

use Contao\Frontend;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Contao\CoreBundle\Framework\ContaoFramework;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Handles front end routes.
 *
 */
class FrontendController extends Frontend
{
  private $contaoFramework;
  private $eventDispatcher;

  public function __construct(ContaoFramework $contaoFramework, EventDispatcherInterface $eventDispatcher)
  {
    $this->contaoFramework = $contaoFramework;
    $this->eventDispatcher = $eventDispatcher;
  }

  public function __invoke(Request $request)
  {
    $this->contaoFramework->initialize();
    $pageModel = $this->contaoFramework->getAdapter(\Contao\PageModel::class);

    $rootPage = $pageModel->findPublishedFallbackByHostname(
        $request->server->get('HTTP_HOST'),
        ['fallbackToEmpty' => true]
    );

    if(!$rootPage->useManifest){
      return new Response('', Response::HTTP_NOT_FOUND);
    }
    $fallbackName = $rootPage->pageTitle?:$rootPage->title;

    $data = array(
      "name"        => $rootPage->manifest_name?:$fallbackName,
      "short_name"  => $rootPage->manifest_shortname?:$fallbackName,
      "lang"        => $rootPage->language,
      "description" => $rootPage->manifest_description
    );
    if($rootPage->manifest_display){
      $data['display'] = $rootPage->manifest_display;
    }
    if($rootPage->manifest_orientation){
      $data['orientation'] = $rootPage->manifest_orientation;
    }
    if($rootPage->manifest_background_color){
      $data['background_color'] = '#'.$rootPage->manifest_background_color;
    }
    if($rootPage->manifest_theme_color){
      $data['theme_color'] = '#'.$rootPage->manifest_theme_color;
    }
    if($rootPage->manifest_icons){
      $data['icons'] = [];

      $filesModel = $this->contaoFramework->getAdapter(\Contao\FilesModel::class);
      foreach(\Contao\StringUtil::deserialize($rootPage->manifest_icons) as $icon){
        $Icon = $filesModel->findByPk($icon);
        if($Icon){
          $File = new \Contao\File($Icon->path);
          $data['icons'][] = array(
            "src" => $Icon->path,
            "sizes" => $File->width."x".$File->height,
            "type" => $File->mime
          );
        }
      }
    }

    $objResponse = new Response(json_encode($data));
    $objResponse->headers->set('Content-Type', 'application/json; charset=UTF-8');
    return $objResponse;
  }
}
