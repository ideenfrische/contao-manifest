<?php

namespace ideenfrische\ContaoManifestBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ideenfrischeContaoManifestExtension extends Extension
{

    /**
     * The files to load.
     *
     * @var string[]
     */
    private $files = [
        'services.yml',
    ];

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        foreach ($this->files as $file) {
            $loader->load($file);
        }
    }
}
