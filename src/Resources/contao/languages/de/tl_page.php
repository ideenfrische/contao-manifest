<?php
  $GLOBALS['TL_LANG']['tl_page']['useManifest']               = ['Webmanifest aktiveren'];
  $GLOBALS['TL_LANG']['tl_page']['manifest_name']             = ['Name','Wenn das Feld leer bleibt, wird automatisch der Seitentitel genutzt.'];
  $GLOBALS['TL_LANG']['tl_page']['manifest_shortname']        = ['Kurzname','Wenn das Feld leer bleibt, wird automatisch der Seitentitel genutzt.'];
  $GLOBALS['TL_LANG']['tl_page']['manifest_description']      = ['Beschreibung'];

  $GLOBALS['TL_LANG']['tl_page']['manifest_background_color'] = ['Hintergrundfarbe'];
  $GLOBALS['TL_LANG']['tl_page']['manifest_theme_color']      = ['Theme-Farbe'];

  $GLOBALS['TL_LANG']['tl_page']['manifest_display']          = ['Anzeige',''];
  $GLOBALS['TL_LANG']['tl_page']['manifest_orientation']      = ['Ausrichtung',''];
  $GLOBALS['TL_LANG']['tl_page']['manifest_icons']            = ['Icons',''];
?>
